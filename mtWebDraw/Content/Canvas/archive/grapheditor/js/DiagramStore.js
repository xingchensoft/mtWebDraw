﻿/**
 * 全部修改重写，不保留原对比
 */
var DiagramStore =
{
    /**
	 * Variable: diagrams
	 * 
	 * Array for in-memory storage of the diagrams. This is not persistent
	 * across multiplace invocations and is only used as a fallback if no
	 * client- or server-side storage is available.
	 */
    eventSource: new mxEventSource(this),

    /**
	 * Function: isAvailable
	 * 
	 * 是否可以保存数据：为true则显示左侧Diagrams面板及保存和另保为按扭。
	 */
    isAvailable: function () {
        return true;
    },

    /**
	 * Function: addListener
	 */
    addListener: function (name, funct) {
        DiagramStore.eventSource.addListener(name, funct);
    },

    /**
	 * Function: removeListener
	 */
    removeListener: function (funct) {
        DiagramStore.eventSource.removeListener(funct);
    },

    /**
	 * Function: put
	 * 
	 * 新增或更新指定Diagram内容
	 */
    put: function (id, name, xml) {
        //对xml没有<mxGraphModel></mxGraphModel>标记而从<root>开始的增加<mxGraphModel></mxGraphModel>
        if (xml && xml.substring(0, 6) == "<root>")
            xml = "<mxGraphModel>" + xml + "</mxGraphModel>";

        var fID = null;
        tabAjaxPostData('/Ajax/CanvasPut', { cid: ChartInfo.cid, id: id, name: name, xml: xml }, function (json) {
            if (json.code == 1) {
                fID = json.data;
            } else {
                errorMessage(json.msg);
            }
        });

        DiagramStore.eventSource.fireEvent(new mxEventObject('put'));
        return fID;
    },

    /**
	 * Function: remove
	 * 
	 * 删除指定Diagram内容
	 */
    remove: function (ID) {
        tabAjaxPost('/Ajax/CanvasRemove?cid=' + ChartInfo.cid + '&id=' + ID, function (json) {
            if (json.code == 1) {
                alertMessage("删除成功！");
            } else {
                errorMessage(json.msg);
            }
        });

        DiagramStore.eventSource.fireEvent(new mxEventObject('remove'));
    },

    /**
	 * Function: get
	 * 
	 * 获取Diagram的绘图图形数据
	 */
    get: function (ID) {
        var xml = null;

        tabAjaxPost('/Ajax/CanvasGet?cid=' + ChartInfo.cid + '&id=' + ID, function (json) {
            if (json.code == 1) {
                xml = json.data.DiagramContent;
            } else {
                errorMessage(json.msg);
            }
        });

        return xml;
    },

    //是否显示删除的内容：0-仅显示未删除，1-仅显示已删除，2-所有
    getNamesIsDelete : "0",

    /**
	 * Function: getNames
	 * 
	 * 获取Diagrams数据列表
	 */
    getNames: function () {
        var names = [];

        tabAjaxPost('/Ajax/CanvasGetNames?cid=' + ChartInfo.cid + "&IsDelete=" + DiagramStore.getNamesIsDelete, function (json) {
            if (json.code == 1) {
                names = json.data;
            } else {
                errorMessage(json.msg);
            }
        });

        return names;
    },

    /**
	 * Function: isExists
	 * 
	 * 查询Diagram名称是否已经存在
	 */
    isExists: function (name) {
        var isEx = true;

        tabAjaxPost('/Ajax/CanvasIsExists?cid=' + ChartInfo.cid + '&name=' + name, function (json) {
            if (json.code == 1) {
                isEx = false;
            } else {
                errorMessage(json.msg);
            }
        });

        return isEx;
    },

    //重命名与备注
    reName: function (ID, name, remark) {
        tabAjaxPostData('/Ajax/CanvasReName', { cid: ChartInfo.cid, id: ID, name: name, remark: remark }, function (json) {
            if (json.code == 1) {
                DiagramStore.eventSource.fireEvent(new mxEventObject('refresh'));
            } else {
                errorMessage(json.msg);
            }
        });
    }

};
